/*Name: Andrew Messinger
Date: 9/22/16
Instructor: Dr. Boatright
Class: COMP322
Purpose: This file defines the functors used in hash.h
and tests functions to see whether a key was found
*/

#include "hash.h"

using namespace std;

//functor for float values
class floatFunctor {
public: 
	int operator()(float x) {
		return static_cast<int>(x*sqrt(2)*1000);
	}
};

//fucntor for int values
class intFunctor {
public: 
	int operator()(int x) {
		return (x%3) + 1;
	}
};

int main() {
	//creating a string functor and testing insert
	cout << "testing stringFunctor w/int return values" << endl;
	Hash<string, int, hash<string>> ex3;
	ex3.insert("yes", 1);
	ex3.insert("no", 4);
	ex3.insert("why", 5);
	
	//testing string functor search
	int check = ex3.search("yes");
	cout << check << endl;
	check = ex3.search("no");
	cout << check << endl;
	check = ex3.search("why");
	cout << check << endl << endl;

	cout << "searching for a key not there" << endl;
	ex3.search("who");

	//creating an int functor and testing insert
	cout << endl << "testing intFunctor w/float return values" << endl;
	Hash<int, float, intFunctor> ex;
	ex.insert(13, 13.5);
	ex.insert(8, 8.5);
	ex.insert(4, 4.5);

	//testing int functor search
	float check2 = ex.search(13);
	cout << check2 << endl;
	check2 = ex.search(8);
	cout << check2 << endl;
	check2 = ex.search(4);
	cout << check2 << endl << endl;

	cout << "searching for a key not there" << endl;
	ex.search(5);

	//creating a float functor and testing search
	cout << endl << "testing floatFunctor w/int return values" << endl;
	Hash<float, int, floatFunctor> ex2;
	ex2.insert(7.5, 7);
	ex2.insert(9.5, 9);
	ex2.insert(3.5, 3);

	//testing float functor search
	int check3 = ex2.search(7.5);
	cout << check3 << endl;
	check3 = ex2.search(9.5);
	cout << check3 << endl;
	check3 = ex2.search(3.5);
	cout << check3 << endl << endl;

	cout << "searching for a key not there" << endl;
	ex2.search(4.5);

	return 0;
}


