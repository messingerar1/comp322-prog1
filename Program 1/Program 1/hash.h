/*Name: Andrew Messinger
Date: 9/22/16
Instructor: Dr. Boatright
Class: COMP322
Purpose: This file is the header for hash.cpp and defines 
the template class Hash and its member functions
*/

#pragma once

#include <iostream>
#include <vector>
#include <utility>
#include <string>
#include <cmath>

using namespace std;

template<class K, class T, class func>
class Hash {
public: 
	//Constructor initializing private members and the 2D vector
	Hash() {
		capacity = 10;
		vecSize = 1;
		loadFactor = 2;

		myPairs.resize(1);
		for (int i = 0; i < 1; i++)
		{
			myPairs[i].resize(capacity);
		}
	}

	//Inserting a key/value pair
	void insert(K keyPar, T valPar) {
		int key = func1(keyPar);
		int location = key%capacity;
		for (int i = 0; i < capacity*loadFactor; i++)
		{
			//Check if the index I want to insert at is 0 (not already taken)
			if (myPairs[i][location].second == NULL)
			{
				//If so, store given key and value in separate 2D vectors and exit loop
				//myPairs[i][location].first = keyPar;
				//myPairs[i][location].second = valPar;
				myPairs[i][location].push_back(keyPar);
				return;
			}
			//If spot is taken and there is no blank row of 0's on the bottom of the 2D matrix, call grow()
			else if (vecSize < i+2)
			{
				grow();
			}
		}
	}

	//doubles the number of rows in the vector
	void grow() {
		myPairs.resize(vecSize * 2);
		myPairs[vecSize].resize(capacity);
		vecSize*=2;
	}


	//determines whether a key is present
	T search(K keyPar) {
		int key = func1(keyPar);
		int location = key%capacity;
		for (int i = 0; i < vecSize; i++)
		{
			if (myPairs[i][location].first == keyPar)
			{
				return myPairs[i][location].second;
			}
		}
		cout << "Key not found." << endl;
		return NULL;
	}

private: 
	int loadFactor;
	int vecSize;
	vector<vector<pair<K, T>>> myPairs;
	int capacity;
	func func1;
};
